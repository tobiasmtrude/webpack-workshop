import Animal from './animal.js';

class Dog extends Animal {

    constructor(name, noOfLegs, weight, race){
        super(name, noOfLegs, weight);
        this.race = race;
    }

    bark() {
        alert("WOOF!");
    }

    generateFactSheet(){
        let dogFactSheet = [];
        const nameListItem = document.createElement('li');
        nameListItem.innerHTML = "Name: " + this.name;
        dogFactSheet.push(nameListItem);

        const raceListItem = document.createElement('li');
        raceListItem.innerHTML = "Hunderasse: " + this.race;
        dogFactSheet.push(raceListItem);

        return dogFactSheet;
    }  

}

export default Dog