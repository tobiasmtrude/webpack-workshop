const loadBear = (bear, imgSrc) => {
    // This function adds the "Show bear" onclick to the button
    const addShowBearClickListener = () => {
        const showBear = () => {
            const container = document.querySelector('#animal-image-container');
            const bearImage = document.createElement('img');
            bearImage.src = imgSrc;
            container.appendChild(bearImage)
        }

        const showBearButton = document.querySelector('#show-bear-button');
        showBearButton.onclick = showBear;
        showBearButton.innerHTML = "Show " + bear.name + "!";
    }

    // This function adds the "Show factsheet" onclick to the button
    const addShowFactsheetClickListener = () => {
        const renderFactsheet = () => {
            const factSheetList = document.querySelector('#factsheet');
            factSheetList.innerHTML = "";
            const bearFacts = bear.generateFactSheet();
            for ( var i = 0; i < bearFacts.length; i++ ) {
                factSheetList.append(bearFacts[i]);
            }
        }
        const factsheetButton = document.querySelector('#show-bear-factsheet');
        factsheetButton.onclick = renderFactsheet;
        factsheetButton.innerHTML = "Show " + bear.name + " facts"
    }

    addShowBearClickListener();
    addShowFactsheetClickListener();
}

export default loadBear;