import loadDog from './dogLoader.js';
import Dog from './dog.js';
import Bear from './bear.js';
import loadBear from './bearLoader.js';

import './styles/index.less';

import BodoImg from  './assets/bodo.jpg';
import KnutImg from './assets/knut.jpg';

const bodo = new Dog("Bodo", 4, 15, "Dackel");
const knut = new Bear("Knut", 4, 40, "Eisbär", "Zoologischer Garten Berlin");

loadDog(bodo, BodoImg);
loadBear(knut, KnutImg);

// Eure Aufgabe: 
// Erstellt eine Webpack-Konfiguration die dieses Skript (und alle dependencies) bundlet und ausführbar macht!

// -Hinweise-
// - Nutzt npm um alle tools zu installieren
// - Man braucht für die hier nötigen Transpile-Schritte...:
//      ES6+ Syntax in ES5 transpilen: babel-loader (@babel/env-preset ist das preset dafür)
//      LESS in CSS transpilen und in JS nutzen: less-loader, css-loader, style-loader
//      Assets (hier .jpg Bilder) in JS importieren: file-loader