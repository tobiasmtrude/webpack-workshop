const loadDog = (dog, imgSrc) => {
    // This function adds the "Show dog" onclick to the button
    const addShowDogClickListener = () => {
        const showDog = () => {
            const container = document.querySelector('#animal-image-container');
            const dogImage = document.createElement('img');
            dogImage.src = imgSrc;
            container.appendChild(dogImage)
        }

        const showDogButton = document.querySelector('#show-dog-button');
        showDogButton.onclick = showDog;
        showDogButton.innerHTML = "Show " + dog.name + "!";
    }

    // This function adds the "Show factsheet" onclick to the button
    const addShowFactsheetClickListener = () => {
        const renderFactsheet = () => {
            const factSheetList = document.querySelector('#factsheet');
            factSheetList.innerHTML = "";
            const dogFacts = dog.generateFactSheet();
            for ( var i = 0; i < dogFacts.length; i++ ) {
                factSheetList.append(dogFacts[i]);
            }
        }
        const factsheetButton = document.querySelector('#show-dog-factsheet');
        factsheetButton.onclick = renderFactsheet;
        factsheetButton.innerHTML = "Show " + dog.name + " facts";
    }

    addShowDogClickListener();
    addShowFactsheetClickListener();
}

export default loadDog;