class Animal {

    constructor(name, noOfLegs, weight) {
        this.name = name;
        this.noOfLegs = noOfLegs;
        this.weight = weight;
    }

    getNoOfLegs() {
        return this.noOfLegs;
    }

    printName() {
        console.log(this.name);
    }

}

export default Animal;