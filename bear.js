import Animal from './animal.js';

class Bear extends Animal {

    constructor(name, noOfLegs, weight, type, habitat){
        super(name, noOfLegs, weight);
        this.type = type;
        this.habitat = habitat;
    }

    rawr() {
        alert("RAWR!");
    }

    generateFactSheet(){
        let bearFactSheet = [];
        const nameListItem = document.createElement('li');
        nameListItem.innerHTML = "Name: " + this.name;
        bearFactSheet.push(nameListItem);

        const typeListItem = document.createElement('li');
        typeListItem.innerHTML = "Bärenart: " + this.type;
        bearFactSheet.push(typeListItem);

        const habitatListItem = document.createElement('li');
        habitatListItem.innerHTML = "Habitat: " + this.habitat;
        bearFactSheet.push(habitatListItem);

        return bearFactSheet;
    }  

}

export default Bear